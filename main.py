import os

# 执行全部用例，知道加载pytest.ini
os.system('pytest -vs --alluredir=.temp/xml')
# 生成allure报告
os.system('allure generate .temp/xml -o .temp/html --clean')
# 打卡报告
os.system('allure open .temp/html')

"""
学习文档：https://www.yuque.com/poloyy/pytest/wubmgb
"""
