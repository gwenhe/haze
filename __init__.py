from sqlalchemy import create_engine, event
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql import select, update, insert, delete


def create_db(database_url: str):
    engine = create_engine(
        database_url, max_overflow=0, pool_size=5, encoding='utf-8',
        # echo=True
    )
    return sessionmaker(bind=engine)()


db = create_db('mysql+pymysql://root:guowenhe@localhost/haze')
""":type: sqlalchemy.orm.Session"""
