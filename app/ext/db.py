import pymysql

conn = pymysql.connect(host='127.0.0.1', user='root', password='guowenhe', database='haze')
# cursor=pymysql.cursors.DictCursor,是为了将数据作为一个字典返回
cursor = conn.cursor(cursor=pymysql.cursors.DictCursor)

sql = 'SELECT * from `user`'
# row返回获取到数据的行数
# 不能将查询的某个表作为一个参数传入到SQL语句中，否则会报错
# eg：sql = 'select id,name from %s'
# eg：row = cursor.excute(sql, 'userinfo') # 备注：userinfo是一个表名
# 像上面这样做就会报SQL语法错误,正确做法如下：
cursor.execute(sql)
# fetchall()（获取所有的数据），fetchmany(size)（size指定获取多少条数据），fetchone()（获取一条数据）
# result = cursor.fetchall()
result = cursor.fetchone()


cursor.close()
conn.close()
print(result)

print(result['name'])

