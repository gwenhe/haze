from sqlalchemy import create_engine, event
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql import select, update, insert, delete


def create_db(database_url: str):
    engine = create_engine(
        database_url, max_overflow=0, pool_size=5, encoding='utf-8',
        # echo=True
    )
    return sessionmaker(bind=engine)()


db = create_db('mysql+pymysql://root:guowenhe@localhost/haze')
""":type: sqlalchemy.orm.Session"""

'sqlacodegen --tables user mysql+pymysql://root:guowenhe@localhost/haze'

from app.models import User


'''
    def get_record(self, order_no: str) -> FinancialHoldingRecord:
        """
        查询单条记录
        :param order_no:
        :return:
        """
        stmt = select(self.model).where(
            self.model.order_no == order_no
        )
        result = self.db.execute(stmt)
        return result.scalars().first()
'''


def get_user_by_id(user_id: int) -> User:
    stmt = select(User).where(
        User.id == user_id
    )
    result = db.execute(stmt)
    return result.scalars().first()


res = get_user_by_id(1)
print(res.six)
print(res.name)
