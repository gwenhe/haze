from pydantic import BaseModel, Field
from typing import Optional, Union, Dict, List
import requests


class RequestSchema(BaseModel):
    url: str = Field(...)
    method: str = Field(...)
    headers: Dict = Field(None)
    body_type: str = Field(...)


class APIRequest(object):
    def __init__(self, request: RequestSchema):
        self.request = request

    def send(self,
             query_params: dict = None,
             data=None,
             json=None,
             headers: dict = None
             ):
        response = requests.request(
            method=self.request.method,
            url=self.request.url,
            params=query_params,
            data=data,
            json=json,
            headers=self.request.headers
        )
        return response


class APIRouter:
    def __init__(self, base_url: str):
        self.base_url = base_url

    def api_route(self,
                  method: str,
                  path: str,
                  body_type: str,
                  ):
        # 6.执行了api_route的函数
        url = self.base_url + path
        # 7.定义了一个属性url，并且赋值了
        api = APIRequest(RequestSchema(
            url=url,
            method=method,
            body_type=body_type,
        ))
        # 8.定义了api 属性， 赋值APIRequest(RequestSchema(
        #             url=url,
        #             method=method,
        #             body_type=body_type,
        #         ))
        # 9。APIRequest(RequestSchema(url=url,method=method,body_type=body_type,))
        #     实例化APIRequest，返回APIRequest对象
        # 10. api = APIRequest类的对象
        return api
        # 11. 返回了api

    def post(self, path: str, body_type: str, description: str):
        # 4.执行了这个函数
        return self.api_route(
            method='POST',
            path=path,
            body_type=body_type
        )
        # 5.返回 调用了实例的api_route
        # 12.拿到了api_route方法的返回数据，并且把数据返回出去

# router = APIRouter(base_url='http://httpbin.org')
# 1.定义了一个属性-router
# 2.给属性-router 赋值- APIRouter(base_url='http://httpbin.org')
# 3.实例化APIRouter类 并传递类APIRouter的init函数定义的数据，返回了一个对象
# 4.router 赋值 APIRouter对象

# req_post = router.post('/post', 'json')
# 1.定义了一个属性-req_post
# 2.给属性-req_post赋值- router.post('/post', 'json')
# 3.  router.post ('/post', 'json')
# 13.req_post是post方法的返回值，而post方法的返回值 是 api_route方法的返回值
#       api_route方法的返回值是APIRequest的对象
#   req_post 是 APIRequest的对象
