from app.ext.api import APIRouter

"""
定义接口
"""

router = APIRouter(base_url='https://apiglobal.likeshuo.online/api')

sign_in = router.post('/SignIn/SignIn', 'json', '用户登录接口')
