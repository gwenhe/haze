"""

写数据库、增删改查
"""
from sqlalchemy.sql import select, update, delete, insert
from app import db
from app.models import User


def get_user_by_id(user_id: int) -> User:
    """
    通过id查询用户
    :param user_id: 用户id
    :return:
    """
    stmt = select(User).where(
        User.id == user_id
    )
    result = db.execute(stmt)
    return result.scalars().first()
