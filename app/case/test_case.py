# import pytest
# import pytest_assume
# import allure
#
# from app import api
#
# pytestmark = [
#     allure.epic("某某系统"),
#     allure.feature('某某平台'),
#     allure.story('某某功能点'),
# ]
#
#
# def func(x):
#     return x + 1
#
#
# def test_answer():
#     assert func(3) == 5
#
#
# class TestClass:
#     @allure.title('**用例名称')
#     def test_one(self, login):
#         api.req_post.send()
#         x = "this"
#         pytest.assume(1 + 4 == 3)
#         pytest.assume(1 + 5 == 3)
#         # assert 1 != 1
#         # assert 1 == 2
#         # assert "h" in x
#
#     def test_two(self):
#         x = "hello"
#         assert hasattr(x, "check")
#
#
# '''
# pytest -vs --alluredir=output/xml
#
# allure generate output/xml -o output/html --clean
#
# allure open output/html
#
#
# '''
