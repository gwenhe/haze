import pytest
import pytest_assume
import allure

from app import api

pytestmark = [
    allure.epic("立刻说系统"),
    allure.feature('教师端'),
]

# 你使用并不一定真的 存在并且正确
# 封装初始化数据的方法，去帮你创建你所需要的账号数据
account = '18665346200'
password = 'w123456'


@allure.story('用户登录-/SignIn/SignIn')
class TestSignIn:

    @allure.title('正确的账号密码登录成功')
    def test_correct_data(self):
        data = {
            "CaptchaId": "",
            "Captcha": "",
            "Randstr": "",
            "Account": account,
            "Password": password,
            "LoginType": 100,
            "StuDoubleCheck": False,
            "EquipmentVersion": "Chrome",
            "AppVersion": "65",
            "UniqueId": "82209e8c058745c8ad1d86648c8776b1",
            "Language": "101",
            "TimeZone": 8,
            "PlatformType": "64"
        }
        # 调用api、获取响应数据.json()
        res_json = api.sign_in.send(json=data).json()
        # 断言
        assert res_json.get('Rcode') == 0
        # assert res_json.get('CName') == '用户名'
        # assert res_json.get('UserId') == '70A63866A0DE3683'

    @allure.title('错误的账号')
    def test_error_account(self):
        error_account = '1234567'
        data = {
            "CaptchaId": "",
            "Captcha": "",
            "Randstr": "",
            "Account": error_account,
            "Password": password,
            "LoginType": 100,
            "StuDoubleCheck": False,
            "EquipmentVersion": "Chrome",
            "AppVersion": "65",
            "UniqueId": "82209e8c058745c8ad1d86648c8776b1",
            "Language": "101",
            "TimeZone": 8,
            "PlatformType": "64"
        }
        # 调用api、获取响应数据.json()
        res_json = api.sign_in.send(json=data).json()
        # 断言
        assert res_json.get('Rcode') == 2017

    @allure.title('正确账号，错误密码')
    def test_error_password(self):
        error_password = '1234567'
        data = {
            "CaptchaId": "",
            "Captcha": "",
            "Randstr": "",
            "Account": account,
            "Password": error_password,
            "LoginType": 100,
            "StuDoubleCheck": False,
            "EquipmentVersion": "Chrome",
            "AppVersion": "65",
            "UniqueId": "82209e8c058745c8ad1d86648c8776b1",
            "Language": "101",
            "TimeZone": 8,
            "PlatformType": "64"
        }
        # 调用api、获取响应数据.json()
        res_json = api.sign_in.send(json=data).json()
        # 断言
        assert res_json.get('Rcode') == 2017
