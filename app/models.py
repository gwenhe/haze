from sqlalchemy import Column, Integer, String
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()
metadata = Base.metadata

"""
写数据库的模型类
"""


class User(Base):
    __tablename__ = 'user'

    id = Column(Integer, primary_key=True, comment='用户id')
    name = Column(String(255), nullable=False, comment='用户姓名')
    six = Column(Integer, nullable=False)
